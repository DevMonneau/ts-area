// utils/dbConnect.js

import mongoose from "mongoose";

const connection = { isConnected: false }; /* creating connection object*/

let uri = process.env.DATABASE_URL

if (!uri) {
  throw new Error(
    "Please define the MONGODB_URI environment variable inside .env.local"
  );
};

async function dbConnect() {
  let db;
  if (!connection.isConnected) {
    db = await mongoose.connect(process.env.DATABASE_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    connection.isConnected = db.connections[0].readyState;
    console.log("-----db isConnected-----", connection.isConnected);
  };
  return db;
};

export default dbConnect;