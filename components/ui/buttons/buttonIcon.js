export default function ButtonBig(props) {
  return (
    <button
      className='bg-white hover:bg-gray-300 text-white font-bold p-1 w-28 h-28 rounded'
      {...props}
    >
      {props.children}
    </button>
  );
}
