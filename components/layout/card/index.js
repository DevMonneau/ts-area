import { useState } from "react";
import {
    Grid,
    Box,
    CardActionArea,
    CardHeader,
    CardContent,
    Typography,
    CardActions,
    Avatar,
    IconButton,
    Container,
} from "@mui/material";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorder from "@mui/icons-material/FavoriteBorder";
import axios from "axios";
import { server } from "../../../tools";

const CardNews = (props) => {
    const [isFav, setFav] = useState(props.news.isLiked);
    //const favorites = props.favoritesNews.filter(element => element = props.news._id) ? console.log(true) : console.log(false);

    console.log(props.favoritesNewss.filter(function (value) {
        if (value === props.news._id) {
            console.log(value, props.news._id)
            return true;
        } else {
            console.log(value, props.news._id)
            return false;
        };
    }));
    
    //console.log(props.favoritesNews, props.news._id)

    const handlefav = async (id) => {
        setFav(!isFav)

        if (isFav) {
            await axios.post(`${server}/api/decrement/${id}`, { withCredentials: true });
        } else {
            await axios.post(`${server}/api/increment/${id}`, { withCredentials: true });
        };
    };

    return (
        <Container>
            <Grid sx={{ width: '50rem', maxWidth: 1000 }}>
                <Box sx={{ my: 1 }} className="border">
                    <CardActionArea>
                        <CardHeader
                            avatar={
                                <Avatar src={props.session.user.image} alt={props.session.user.name} aria-label="recipe" />
                            }
                            title={`${props.news.userId}`}
                            subheader={moment(props.news.postedAt).format("LL")}
                        />
                        <CardContent>
                            <Typography variant="body2" color="text.secondary">
                                {props.news.body}
                            </Typography>
                        </CardContent>
                        <CardActions disableSpacing sx={{ display: "flex", justifyContent: "flex-end" }}>
                            <IconButton aria-label="add to favorites" onClick={() => handlefav(props.news._id, setFav, isFav)}>
                                {props.news.likes > 0 && <Typography>{`${props.news.likes}`}</Typography>}
                                {isFav ? <FavoriteIcon sx={{ color: "red" }} /> : <FavoriteBorder />}
                            </IconButton>
                        </CardActions>
                    </CardActionArea>
                </Box>
            </Grid>
        </Container>
    );
};

export default CardNews;