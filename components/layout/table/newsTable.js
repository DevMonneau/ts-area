import { useState } from "react";
import { server } from "../../../tools";
import { DataGrid } from '@mui/x-data-grid';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from "@mui/material";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import moment from 'moment';
import axios from "axios";

const NewsTable = (props) => {
    const [open, setOpen] = useState(false);
    const [idToDelete, setIdToDelete] = useState();

    const columns = [
        {
            field: 'postedAt',
            headerName: 'Date',
            width: 250,
            renderCell: (params) =>
                <p>{moment(params.row.postedAt).format("LL")}</p>
        },
        { field: 'body', headerName: 'Body', width: 500 },
        { field: 'userId', headerName: 'Posted by', type: 'string', width: 200 },
        {
            field: "action",
            headerName: "Delete",
            width: 100,
            renderCell: (params) =>
                <div>
                    <DeleteForeverIcon onClick={() => {
                        setOpen(!open), setIdToDelete(params.row._id)
                    }} />
                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">
                            {"Are you absolutely sure?"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                This action cannot be undone. This will permanently delete the new.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Close</Button>
                            <Button onClick={handleDelete} autoFocus>
                                Delete
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
        },
    ];

    const handleClose = () => {
        setOpen(false);
    };

    const handleDelete = () => {
        axios.delete(`${server}/api/news/${idToDelete}`, { withCredentials: true })
            .then(() => {
                setOpen(!open);
                window.location.reload(false);
                /*                 axios.get(`${server}/api/news`, { withCredentials: true })
                                    .then(res => {
                                        console.log(res)
                                    }) */
            });
    };

    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid getRowId={(row) => row._id} rows={props.news} columns={columns} />
        </div>
    );
};

export default NewsTable;
