import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const columns = [
    { field: 'accountGame', headerName: 'VideoGame', type: "string", width: 190 },
    { field: 'accountName', headerName: 'Account name', width: 190 },
    { field: 'accountPwd', headerName: 'Password', width: 190 },
    { field: 'accountId', headerName: 'ID', width: 190 },
    { field: 'accountRank', headerName: 'Rank', width: 190 },
    { field: 'userId', headerName: 'Posted by', width: 190 },
];

export default function AccountTable(props) {
    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid
                getRowId={(row) => row._id}
                rows={props.accounts}
                columns={columns}
                components={{ Toolbar: props.QuickSearchToolbar }}
                componentsProps={{
                    toolbar: {
                        value: props.searchText,
                        onChange: (e) => props.requestSearch(e.target.value),
                        clearSearch: () => props.requestSearch(""),
                    }
                }}
            />
        </div>
    );
};
