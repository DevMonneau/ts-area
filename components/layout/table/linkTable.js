import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const LinkTable = (props) => {
    const columns = [
        { field: 'category', headerName: 'Category', width: 270 },
        {
            field: 'link',
            headerName: 'Link',
            width: 600,
            renderCell: (params) =>
                <a href={params.row.link} target="_blank" rel="noreferrer">{params.row.link}</a>
        },
        { field: 'userId', headerName: 'Posted by', type: 'string', width: 270 },
    ];

    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid
                getRowId={(row) => row._id}
                rows={props.links}
                columns={columns}
                components={{ Toolbar: props.QuickSearchToolbar }}
                componentsProps={{
                    toolbar: {
                        value: props.searchText,
                        onChange: (event) => props.requestSearch(event.target.value),
                        clearSearch: () => props.requestSearch(''),
                    }
                }}
            />
        </div>
    );
};

export default LinkTable;
