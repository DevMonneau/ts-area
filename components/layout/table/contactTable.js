import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import {
    Avatar,
    Box,
} from "@mui/material";

export default function ContactTable(props) {
    const columns = [
        {
            field: "avatar",
            headerName: "Avatar",
            width: 240,
            renderCell: () => {
                return (
                    <Box>
                        <Avatar src={props.session.user.image} alt={props.session.user.name} sx={{ display: "flex", alignItems: "center" }} />
                    </Box>
                );
            }
        },
        { field: 'twitter', headerName: 'Twitter', type: "string", width: 224 },
        { field: 'instagram', headerName: 'Instagram', width: 224 },
        { field: 'phoneNumber', headerName: 'Phone Number', width: 224 },
        { field: 'userId', headerName: 'Contributor', type: 'string', width: 224 },
    ];

    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid
                getRowId={(row) => row._id}
                rows={props.contacts}
                columns={columns}
                components={{ Toolbar: props.QuickSearchToolbar }}
                componentsProps={{
                    toolbar: {
                        value: props.searchText,
                        onChange: (e) => props.requestSearch(e.target.value),
                        clearSearch: () => props.requestSearch("",)
                    },
                }}
            />
        </div>
    );
}
