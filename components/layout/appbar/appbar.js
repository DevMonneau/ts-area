import { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import { server } from "../../../tools";
import {
  Avatar,
  Box,
  AppBar,
  CssBaseline,
  Toolbar,
  IconButton,
  Tooltip,
  Typography,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import BarDrawer from './barDrawer';
import ProfilBar from "./profilBar";
import { useSession } from "next-auth/react";
import axios from 'axios';
import Image from "next/image";
const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(2),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  })
);

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const ClippedDrawer = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [areaName, setAreaName] = useState();
  const { data: session, status } = useSession();

  useEffect(() => {
    axios.get(`${server}/api/area/${session.areaSelected}`, { withCredentials: true })
      .then(res => {
        setAreaName(res.data.area.name)
      })
  }, []);

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const handleProfilOpen = () => {
    setIsShow(!isShow);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar
        position='fixed'
        sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
      >
        <Toolbar sx={{ background: "#212121", height: 50, display: "flex", justifyContent: "space-between" }}>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            onClick={handleDrawerOpen}
            edge='start'
            sx={{ mr: 2 }}
          >
            <Image alt="Mountains" src="/tsarealogo-grey.png" height={50} width={50} />
            {/*             <MenuIcon />
 */}            <Typography
              variant='h6'
              sx={{ m: 4 }}
            >
              {areaName}
            </Typography>
          </IconButton>
          <Tooltip title="Open settings">
            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
              <Avatar alt={session.user.name} src={session.user.image} onClick={handleProfilOpen} />
            </IconButton>
          </Tooltip>

        </Toolbar>
      </AppBar>
      <BarDrawer open={open} />
      <ProfilBar isShow={isShow} anchorElUser={anchorElUser} setAnchorElUser={setAnchorElUser} />
      <Main open={open}>
        <DrawerHeader />
        {children}
      </Main>
    </Box>
  );
};

export default ClippedDrawer;
