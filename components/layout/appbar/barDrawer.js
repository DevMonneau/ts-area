import { useRouter } from 'next/dist/client/router';
import {
  List,
  Toolbar,
  Drawer,
  Box,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material/';
import {
  InsertLink,
  SwitchAccount,
  EmojiObjects,
  Settings,
  Announcement,
  PermContactCalendar,
  Home,
  Menu,
} from '@mui/icons-material';

const drawerWidth = 240;

const BarDrawer = (props) => {
  const router = useRouter();
  const handleRedirect = (path) => {
    router.push(path);
  };

  return (
    <Drawer
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        '& .MuiDrawer-paper': {
          width: drawerWidth,
          boxSizing: 'border-box',
          background: "#424242",
          color: "white"
        },
      }}
      variant='persistent'
      anchor='left'
      open={props.open}
    >
      <Toolbar />
      <Box sx={{ overflow: 'auto' }}>
        <List>
          <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "90vh" }}>
            <Box>
              <ListItemButton onClick={() => handleRedirect('/dashboard')}>
                <ListItemIcon>
                  <Home sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Home'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/account')}>
                <ListItemIcon>
                  <SwitchAccount sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Accounts'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/links')}>
                <ListItemIcon>
                  <InsertLink sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Links'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/tuto')}>
                <ListItemIcon>
                  <EmojiObjects sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Tutorials'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/gamesettings')}>
                <ListItemIcon>
                  <Settings sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Game config'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/news')}>
                <ListItemIcon>
                  <Announcement sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'News'} />
              </ListItemButton>
              <ListItemButton onClick={() => handleRedirect('/contacts')}>
                <ListItemIcon>
                  <PermContactCalendar sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Contact'} />
              </ListItemButton>
            </Box>
            <Box>
              <ListItemButton onClick={() => handleRedirect('/area')}>
                <ListItemIcon>
                  <Menu sx={{ color: "white" }} />
                </ListItemIcon>
                <ListItemText primary={'Change area'} />
              </ListItemButton>
            </Box>
          </Box>
        </List>
      </Box>
    </Drawer >
  );
};

export default BarDrawer;
