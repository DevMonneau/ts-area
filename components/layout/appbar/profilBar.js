import { useState } from "react";
import { useRouter } from "next/dist/client/router";
import {
    Box,
    ListItemButton,
    ListItemText,
    Menu,
    MenuItem,
    Toolbar,
    Typography,
} from "@mui/material";
import { signOut } from 'next-auth/react';

const BarProfil = (props) => {
    const [anchorElNav, setAnchorElNav] = useState(null);
    const router = useRouter();

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        props.setAnchorElUser(null);
    };

    const handleRedirect = (path) => {
        router.push(path);
    };

    return (
        <>
            <Menu
                id="menu-appbar"
                anchorEl={props.anchorElUser}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={Boolean(props.anchorElUser)}
                onClose={handleCloseUserMenu}
                sx={{ mt: 5.5 }}
            >
                <MenuItem onClick={handleCloseNavMenu}>
                    <Typography onClick={() => handleRedirect("/profil")}>Profile</Typography>
                </MenuItem>
                <MenuItem onClick={handleCloseNavMenu}>
                    <Typography onClick={() => handleRedirect("/area-settings")}>Area settings</Typography>
                </MenuItem>
                <MenuItem onClick={handleCloseNavMenu}>
                    <Typography onClick={() => signOut()}>Logout</Typography>
                </MenuItem>
            </Menu>
        </>
    );
};

export default BarProfil