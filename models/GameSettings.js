import mongoose from "mongoose";

const GameSettingsSchema = new mongoose.Schema({
    image: { type: String, default: "", required: false },
    areaId: { type: mongoose.Types.ObjectId, ref: "Area" },
    userId: { type: mongoose.Types.ObjectId, ref: "User" },
});

module.exports =
    mongoose.models.GameSettings ||
    mongoose.model("GameSettings", GameSettingsSchema);
