import mongoose from "mongoose";

const ContactSchema = new mongoose.Schema({
    twitter: { type: String, required: false },
    instagram: { type: String, required: false },
    phoneNumber: { type: String, required: false },
    areaId: { type: mongoose.Types.ObjectId, ref: "Area" },
    userId: { type: mongoose.Types.ObjectId, ref: "User" },
});

module.exports =
    mongoose.models.Contact ||
    mongoose.model("Contact", ContactSchema);
