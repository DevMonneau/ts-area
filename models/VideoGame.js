import mongoose from "mongoose";

const VideoGameSchema = new mongoose.Schema({
    name: { type: String, unique: true, required: true },
});

module.exports =
    mongoose.models.VideoGame ||
    mongoose.model("VideoGame", VideoGameSchema);
