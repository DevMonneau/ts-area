import mongoose from "mongoose";

const AreaSchema = new mongoose.Schema({
    name: { type: String, required: true },
    createdAt: { type: Date, required: false },
    createdBy: { type: mongoose.Types.ObjectId, ref: "User", required: true },
    //sharedAccountId: [{ type: mongoose.Types.ObjectId, ref: "SharedAccount" }],
    //linksId: [{ type: mongoose.Types.ObjectId, ref: "Links" }],
    //newsId: [{ type: mongoose.Types.ObjectId, ref: "News" }],
    //tutoId: [{ type: mongoose.Types.ObjectId, ref: "Tuto" }],
});

module.exports =
    mongoose.models.Area ||
    mongoose.model("Area", AreaSchema);