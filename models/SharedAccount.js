import mongoose from 'mongoose';

const SharedAccount = new mongoose.Schema({
    accountName: { type: String, required: true },
    accountPwd: { type: String, required: true },
    createdAt: { type: String, required: true },
    accountId: { type: String, required: false },
    accountRank: { type: String, required: true },
    accountGame: { type: mongoose.Schema.Types.ObjectId, ref: "VideoGame", required: true },
    // accountPlayable: { type: Boolean },
    areaId: { type: mongoose.Schema.Types.ObjectId, ref: "Area" },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
});

module.exports =
    mongoose.models.SharedAccount ||
    mongoose.model('SharedAccount', SharedAccount);
