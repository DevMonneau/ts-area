import mongoose from "mongoose";

const TutoSchema = new mongoose.Schema({
    link: { type: String, required: true },
    category: { type: mongoose.Types.ObjectId, ref: "Category", required: true },
    areaId: { type: mongoose.Types.ObjectId, ref: "Area", require: true },
    userId: { type: mongoose.Types.ObjectId, ref: "User" },
});

module.exports =
    mongoose.models.Tuto ||
    mongoose.model("Tuto", TutoSchema);
