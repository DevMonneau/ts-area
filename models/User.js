import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: false },
  email: { type: String, unique: true, required: true },
  createdAt: { type: Date, required: false },
  area: [{ type: mongoose.Types.ObjectId, ref: "area" }],
  areaSelected: { type: mongoose.Types.ObjectId, ref: "area" },
  favoriteNews: [{ type: mongoose.Types.ObjectId, ref: "news" }],
})

module.exports =
  mongoose.models.User ||
  mongoose.model("User", UserSchema);
