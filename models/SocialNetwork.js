import mongoose from "mongoose";

const SocialNetworkSchema = new mongoose.Schema({
    name: { type: String, required: true },
});

module.exports =
    mongoose.models.SocialNetwork ||
    mongoose.model("SocialNetwork", SocialNetworkSchema);
