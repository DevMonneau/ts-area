import mongoose from "mongoose";

const NewsSchema = new mongoose.Schema({
    body: { type: String, required: true },
    postedAt: { type: String, required: true },
    areaId: { type: mongoose.Types.ObjectId, ref: "Area", required: false },
    userId: { type: mongoose.Types.ObjectId, ref: "User", required: false },
    likes: { type: Number, default: 0 },
    isLiked: { type: Boolean, required: true }
});

module.exports =
    mongoose.models.News ||
    mongoose.model("News", NewsSchema);
