import mongoose from "mongoose";

const LinksSchema = new mongoose.Schema({
    link: { type: String, required: true },
    category: { type: mongoose.Types.ObjectId, ref: "Category", required: true },
    areaId: { type: mongoose.Types.ObjectId, ref: "Area", required: false },
    userId: { type: mongoose.Types.ObjectId, ref: "User", required: false },
});

module.exports =
    mongoose.models.Link ||
    mongoose.model("Link", LinksSchema);
