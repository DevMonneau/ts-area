const getPath = (path) => {
  const env = process.env.NODE_ENV;
  let url =
    env === 'production'
      ? process.env.NEXT_PUBLIC_URL
      : process.env.NEXT_PUBLIC_URL_DEV;

  console.log(url + path);

  return url + path;
};

export default getPath;
