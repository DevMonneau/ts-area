import getPath from './getPath';

const routeProtected = (session) => {
  window.location.href = getPath('/auth/signin');
};

export default routeProtected;
