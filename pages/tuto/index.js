import { useState, useEffect } from "react";
import AppBar from '../../components/layout/appbar';
import TutoTable from "../../components/layout/table/tutoTable";
import { server } from '../../tools';
import Head from 'next/head';
import axios from 'axios';
import {
  Button,
  CssBaseline,
  TextField,
  Grid,
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,
} from "@mui/material";
import {
  GridToolbarDensitySelector,
  GridToolbarFilterButton,
} from '@mui/x-data-grid';
import PropTypes from 'prop-types';

const escapeRegExp = (value) => {
  return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
};

const QuickSearchToolbar = () => {
  return (
    <Box
      sx={{
        p: 0.5,
        pb: 0,
        justifyContent: 'space-between',
        display: 'flex',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
      }}
    >
      <div>
        <GridToolbarFilterButton />
        <GridToolbarDensitySelector />
      </div>
    </Box>
  );
};

QuickSearchToolbar.propTypes = {
  clearSearch: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default function Tuto() {
  const [selectedCategory, setSelectedCategory] = useState();
  const [data, setData] = useState();
  const [searchText, setSearchText] = useState();
  const [categories, setCategories] = useState([]);
  const [tutos, setTutos] = useState([]);

  const requestSearch = (searchValue) => {
    setSearchText(searchValue);
    const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
    const filteredRows = tutos.filter((row) => {
      return Object.keys(row).some((field) => {
        return searchRegex.test(row[field].toString());
      });
    });
    setRows(filteredRows);
  };

  useEffect(() => {
    axios.get(`${server}/api/tuto`, { withCredentials: true })
      .then(res => {
        setTutos(res.data.newFormat);
      });

    axios.get(`${server}/api/category`, { withCredentials: true })
      .then(res => {
        console.log("res", res)
        setCategories(res.data.categories);
      });
  }, []);

  const preventDefault = (e) => {
    e.preventDefault();
  };

  const handleSubmit = () => {
    const fetchDatabase = async () => {
      const res = await axios.post(
        `${server}/api/tuto`,
        { link: data, category: selectedCategory },
      );
      if (res.data.status === "Success") {
        axios.get(`${server}/api/tuto`, { withCredentials: true })
          .then(res => {
            setTutos(res.data.newFormat);
          });
      };
    };
    fetchDatabase();
  };

  return (
    <div>
      <Head>
        <title>TS AREA - TUTO</title>
      </Head>

      <AppBar>
        <Container component="main"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",

          }}
          onSubmit={preventDefault}
        >
          <CssBaseline />

          <Grid sx={{ m: 1 }}>
            <FormControl>
              <InputLabel id="category">Category</InputLabel>
              <Select
                defaultValue
                labelId="category"
                id="category"
                value={categories.name}
                label="category"
                onChange={e => setSelectedCategory(e.target.value)}
                sx={{ width: 200 }}
              >
                {
                  categories.map(item => (
                    <MenuItem value={item._id} key={item._id}>{item.name}</MenuItem>
                  ))
                }
              </Select>
            </FormControl>
          </Grid>
          <Grid sx={{ m: 1 }} xs={6}>
            <TextField fullWidth id="outlined-basic" label="Link" variant="outlined" onChange={e => setData(e.target.value)} />
          </Grid>
          <Grid sx={{ m: 1 }}>
            <Button variant="contained" size="medium" onClick={handleSubmit}>Send</Button>
          </Grid>
        </Container>
        <Container>
          <Grid sx={{ m: 5 }}>
            <TutoTable tutos={tutos} requestSearch={requestSearch} QuickSearchToolbar={QuickSearchToolbar} searchText={searchText} />
          </Grid>
        </Container>
      </AppBar>
    </div>
  );
};

Tuto.auth = true;
