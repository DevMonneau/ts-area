import { useEffect } from "react";
import { SessionProvider, useSession, signIn } from "next-auth/react";
import { useRouter } from "next/router";

import '../styles/globals.css';
import "../styles/styles.css";
import "../styles/Area.modules.css";
import "../styles/New-area.modules.css";
import "../styles/Dashboard.modules.css";

import { ThemeProvider, createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: "#505050",
    },
  },
});

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}) {
  return (
    <SessionProvider session={session}>
      <ThemeProvider theme={theme}>
        {Component.auth ? (
          <Auth>
            <Component {...pageProps} />
          </Auth>
        ) : (
          <Component {...pageProps} />
        )}
      </ThemeProvider>
    </SessionProvider>
  );
};

function Auth({ children }) {
  const { data: session, status } = useSession();
  const isUser = !!session?.user;
  const router = useRouter();

  useEffect(() => {
    if (status === "loading") return // Do nothing while loading
    if (!isUser) signIn() // If not authenticated, force log in
  }, [isUser, status, router]);

  if (isUser /* && isArea */) {
    return children
  };

  // Session is being fetched, or no user.
  // If no user, useEffect() will redirect.
  return <div>Loading...</div>
};
