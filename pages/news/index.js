import { useState, useEffect } from 'react';
import { server } from "../../tools";
import { useSession } from "next-auth/react"
import axios from 'axios';
import AppBar from '../../components/layout/appbar';
import Head from "next/head";
import {
    Box,
    Grid,
    Container,
    Button,
    Avatar,
    FormControl,
    InputLabel,
    Input,
} from "@mui/material";
import NewsTable from '../../components/layout/table/newsTable';

const News = () => {
    const [news, setNews] = useState([]);
    const [body, setBody] = useState();
    const { data: session, status } = useSession();

    useEffect(() => {
        axios.get(`${server}/api/news`, { withCredentials: true })
            .then(res => {
                console.log(res)
                setNews(res.data.news);
            })
    }, []);

    const handleSubmit = () => {
        const fetchDatabase = async () => {
            const res = await axios.post(
                `${server}/api/news`,
                { body },
            );
            if (res.data.status === "Success") {
                axios.get(`${server}/api/news`, { withCredentials: true })
                    .then(res => {
                        setNews(res.data.newsByUserId);
                    });
            };
        };
        fetchDatabase();
    };

    return (
        <div>
            <Head>
                <title>TS AREA - NEWS</title>
            </Head>
            <AppBar>
                <Container component="main">
                    <Grid
                        sx={{
                            display: 'flex',
                            flexDirection: "column",
                            alignItems: 'center',
                        }}
                    >
                        <Box>
                            <Box sx={{ my: 3, mx: 2 }}>
                                <Avatar alt={session.user.name} src={session.user.image} />
                                <FormControl variant="standard">
                                    <InputLabel htmlFor="standard-adornment-amount">What s new</InputLabel>
                                    <Input
                                        sx={{ width: "30rem" }}
                                        id="standard-adornment-amount"
                                        onChange={e => setBody(e.target.value)}
                                    />
                                </FormControl>
                            </Box>
                            <Box sx={{ mx: 2 }}>
                                <Button variant="contained" size="small" onClick={handleSubmit}>Post</Button>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid sx={{ m: 3 }}>
                        <NewsTable news={news} session={session} />
                    </Grid>
                </Container>
            </AppBar>
        </div>
    );
};

News.auth = true;

export default News;
