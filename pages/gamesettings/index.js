import { useState, useEffect } from "react";
import { server } from "../../tools";
import axios from "axios";
import AppBar from "../../components/layout/appbar";
import Head from "next/head";
import Image from "next/image";
import {
    Container,
    Box,
    Typography,
    Button,
} from "@mui/material";
import FileDownloadIcon from '@mui/icons-material/FileDownload';

const GameSettings = () => {
    const [imageURL, setURL] = useState();
    //const [createObjectURL, setCreateObjectURL] = useState(null);
    const [data, setData] = useState();

    useEffect(() => {
        axios.get(`${server}/api/gameSettings`, { withCredentials: true })
            .then(res => {
                setData(res.data.gameSettings);
            });
    }, []);

    console.log("data", data)

    const handleSendImage = async () => {
        await axios.post(`${server}/api/uploadFile`, { image: imageURL }, { withCredentials: true })
            .then(res => {
                console.log(res);
                /*                if (res.data.status === "Success") {
                                   //setCreateObjectURL(URL.createObjectURL(imageURL));
               
                                   setTimeout(() => {
                                       handleImage(res.data.gameSettings._id)
                                   })
                               }; */
            });
    };

    const handleImage = async (id) => {
        const body = new FormData();
        body.append("file", imageURL);

        axios.patch(`${server}/api/gameSettings/${id}`, { withCredentials: true })
            .then(res => {
                console.log("res for patch method", res);
            });
    };

    if (!data) {
        return (
            <div>Loading...</div>
        );
    };

    return (
        <div>
            <Head>
                <title>TS AREA - GAMESETTINGS</title>
            </Head>

            <AppBar>
                <Container component="main">


                    <Box sx={{ display: "flex", justifyContent: "center" }} >
                        <form action="/api/uploadFile" encType="multipart-formdata" method="POST">
                            <input type="file" name="file" id="file" className="inputfile" onChange={(e) => setURL(e.target.files[0].name)} />
                            <label htmlFor="file">
                                <Box className="display">
                                    <FileDownloadIcon />
                                    <Typography>Choose your file</Typography>
                                </Box>
                            </label>
                        </form>
                    </Box>
                    <Box>
                        <Button variant="contained" size="medium" onClick={handleSendImage}>Send</Button>
                    </Box>
                    {/*                     <Image src={data} alt="test" width={500} height={500} />
 */}                </Container>
            </AppBar>
        </div>
    );
};

GameSettings.auth = true;

export default GameSettings;
