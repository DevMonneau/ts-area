import { useState, useEffect } from "react";
import AppBar from "../../components/layout/appbar";
import Head from "next/head";
import axios from "axios";
import { server } from "../../tools";
import {
    Container,
    CssBaseline,
    Grid,
    TextField,
    Button,
    Box,
} from "@mui/material";
import ContactTable from "../../components/layout/table/contactTable";
import { useSession } from "next-auth/react";
import {
    GridToolbarDensitySelector,
    GridToolbarFilterButton,
} from '@mui/x-data-grid';
import PropTypes from "prop-types";

const escapeRegExp = (value) => {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
};

const QuickSearchToolbar = () => {
    return (
        <Box
            sx={{
                p: 0.5,
                pb: 0,
                justifyContent: 'space-between',
                display: 'flex',
                alignItems: 'flex-start',
                flexWrap: 'wrap',
            }}
        >
            <div>
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
            </div>
        </Box>
    );
};

QuickSearchToolbar.propTypes = {
    clearSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

const Contacts = () => {
    const [twitter, setTwitter] = useState();
    const [instagram, setInstagram] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [contacts, setContacts] = useState([]);
    const [searchText, setSearchText] = useState();
    const { data: session, status } = useSession();

    useEffect(() => {
        axios.get(`${server}/api/contact`, { withCredentials: true })
            .then(res => {
                setContacts(res.data.contacts);
            });
    }, []);

    const preventDefault = (e) => {
        e.preventDefault();
    };

    const requestSearch = (searchValue) => {
        setSearchText(searchValue);
        const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
        const filteredRows = contacts.filter(row => {
            return Object.keys(row).some(field => {
                return searchRegex.test(row[field].toString());
            });
        });
    };

    const handleSubmit = () => {
        const fetchDatabase = async () => {
            const res = await axios.post(
                `${server}/api/contact`,
                { twitter, instagram, phoneNumber },
            );
            console.log(res);
            if (res.data.status === "Success") {
                axios.get(`${server}/api/contact`, { withCredentials: true })
                    .then(res => {
                        setContacts(res.data.contacts);
                    });

            }
        };
        fetchDatabase();
    };

    return (
        <div>
            <Head>
                <title>TS AREA - CONTACTS</title>
            </Head>
            <AppBar>
                <Container component="main">
                    <CssBaseline />
                    <Grid container
                        spacing={0}
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        onSubmit={preventDefault}
                    >
                        <Grid sx={{ m: 1 }}>
                            <TextField id="twitter" label="Twitter" variant="outlined" onChange={e => setTwitter(e.target.value)} />
                        </Grid>
                        <Grid sx={{ m: 1 }}>
                            <TextField id="instagram" label="Instagram" variant="outlined" onChange={e => setInstagram(e.target.value)} />
                        </Grid>
                        <Grid sx={{ m: 1 }}>
                            <TextField id="phone_number" label="Phone Number" variant="outlined" onChange={e => setPhoneNumber(e.target.value)} />
                        </Grid>
                        <Grid sx={{ m: 1 }}>
                            <Button variant="contained" size="medium" onClick={handleSubmit}>Send</Button>
                        </Grid>
                    </Grid>
                    <Grid sx={{ mt: 5 }}>
                        <ContactTable contacts={contacts} requestSearch={requestSearch} QuickSearchToolbar={QuickSearchToolbar} session={session} searchText={searchText} />
                    </Grid>
                </Container>
            </AppBar>
        </div>
    );
};

Contacts.auth = true;

export default Contacts;
