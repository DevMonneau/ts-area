import { useState, useEffect } from "react";
import { server } from "../../tools";
import Head from "next/head";
import AppBar from "../../components/layout/appbar";
import TableData from "../../components/layout/table/linkTable";
import {
    Button,
    CssBaseline,
    TextField,
    Grid,
    Container,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Box,
} from "@mui/material"
import axios from "axios";
import {
    GridToolbarDensitySelector,
    GridToolbarFilterButton,
} from '@mui/x-data-grid';
import PropTypes from 'prop-types';

const escapeRegExp = (value) => {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
};

const QuickSearchToolbar = () => {
    return (
        <Box
            sx={{
                p: 0.5,
                pb: 0,
                justifyContent: 'space-between',
                display: 'flex',
                alignItems: 'flex-start',
                flexWrap: 'wrap',
            }}
        >
            <div>
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
            </div>
        </Box>
    );
};

QuickSearchToolbar.propTypes = {
    clearSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};
export default function Links() {
    const [data, setData] = useState();
    const [categories, setCategories] = useState([]);
    const [links, setLinks] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState();
    const [searchText, setSearchText] = useState();

    useEffect(() => {
        axios.get(`${server}/api/category`, { withCredentials: true })
            .then(res => {
                setCategories(res.data.categories);
            });

        axios.get(`${server}/api/links`, { withCredentials: true })
            .then(res => {
                setLinks(res.data.newFormat);
            });
    }, [])

    const preventDefault = (e) => {
        e.preventDefault();
    };

    const requestSearch = (searchValue) => {
        setSearchText(searchValue);
        const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
        const filteredRows = tutos.filter((row) => {
            return Object.keys(row).some((field) => {
                return searchRegex.test(row[field].toString());
            });
        });
    };

    const handleSubmit = () => {
        const fetchDatabase = async () => {
            const res = await axios.post(
                `${server}/api/links`,
                { link: data, category: selectedCategory },
            );
            if (res.data.status === "Success") {
                axios.get(`${server}/api/links`, { withCredentials: true })
                    .then(res => {
                        setLinks(res.data.newFormat);
                    });
            };
        };
        fetchDatabase();
    };

    return (
        <div>
            <Head>
                <title>TS AREA - LINKS</title>
            </Head>
            <AppBar>
                <Container component="main">
                    <CssBaseline />
                    <Grid container
                        spacing={0}
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        onSubmit={preventDefault}
                    >
                        <Grid sx={{ m: 1 }}>
                            <FormControl >
                                <InputLabel id="category">Category</InputLabel>
                                <Select
                                    defaultValue
                                    labelId="category"
                                    id="category"
                                    value={categories.name}
                                    label="Category"
                                    onChange={e => setSelectedCategory(e.target.value)}
                                    sx={{ width: 200 }}
                                >
                                    {
                                        categories.map(item => (
                                            <MenuItem value={item._id} key={item._id}>{item.name}</MenuItem>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid sx={{ m: 1 }} xs={6}>
                            <TextField fullWidth id="outlined-basic" label="Link" variant="outlined" onChange={e => setData(e.target.value)} />
                        </Grid>
                        <Grid sx={{ m: 1 }} >
                            <Button variant="contained" size="medium" onClick={handleSubmit}>Send</Button>
                        </Grid>
                    </Grid>
                    <Grid sx={{ mt: 5 }}>
                        <TableData links={links} requestSearch={requestSearch} QuickSearchToolbar={QuickSearchToolbar} searchText={searchText} />
                    </Grid>
                </Container>
            </AppBar >
        </div>
    );
};

Links.auth = true;


