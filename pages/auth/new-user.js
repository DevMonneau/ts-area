import Head from "next/head";
import { Typography } from "@mui/material"

const NewUser = () => {
    return (
        <>
            <Head>
                <title>TS AREA - NewUser</title>
            </Head>
            <Typography>
                NewUser
            </Typography>
        </>
    );
};

export default NewUser;