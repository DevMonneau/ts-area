import Head from "next/head";
import { Typography } from "@mui/material"

const Logout = () => {
    return (
        <>
            <Head>
                <title>TS AREA - Logout</title>
            </Head>
            <Typography>
                Logout
            </Typography>
        </>
    );
};

export default Logout;