import Head from "next/head";
import { Typography } from "@mui/material"

const Error = () => {
    return (
        <>
            <Head>
                <title>TS AREA - Error</title>
            </Head>
            <Typography>
                Error
            </Typography>
        </>
    );
};

export default Error;