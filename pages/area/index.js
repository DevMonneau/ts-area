import { useEffect, useState } from "react";
import Image from "next/image";
import Head from "next/head";
import { getSession, useSession } from "next-auth/react";
import { useRouter } from 'next/router';
import { server } from "../../tools";
import axios from "axios";
import {
    Button,
    Container,
    Typography,
    CardActionArea,
    Box,
    Avatar,
} from "@mui/material";
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

const Area = () => {
    const [areas, setAreas] = useState([]);
    const router = useRouter();
    const { data: session, status } = useSession();

    useEffect(() => {
        axios.get(`${server}/api/area`, { withCredentials: true })
            .then(res => {
                setAreas(res.data.areas);
            });
    }, []);

    const fetchDatabase = async (id) => {
        const res = await axios.get(
            `${server}/api/area/${id}`,
            { withCredentials: true, headers: { "areaId": id } },
        );
        if (res.data.status === "Success") {
            router.push("/dashboard");
        };
    };
    return (
        <>
            <Head>
                <title>TS AREA - Area</title>
            </Head>

            <Box component="main" className="area-container">
                <Container className="logo-container">
                    <Image alt="Mountains" src="/tsarealogo.png" height={200} width={200} className="margin-area-1rem" />
                    <Typography variant="h6" className="margin-area-1rem">Create a new area</Typography>
                    <Typography className="margin-area-1rem">We  propose a place for gamers to share information like smurfs, tutorials and much more. TS Area is a place for community sharing.</Typography>
                    <Button
                        variant="contained"
                        onClick={() => { router.push("/new-area") }}
                        className="margin-area-1rem"
                    >Create an area
                    </Button>
                </Container>
            </Box>
            {
                areas.length > 0 &&
                <Container className="test">
                    <Box className="or-container">
                        <Typography className="or-text">or</Typography>
                    </Box>
                    <Typography className="margin-area-1rem">Select an area</Typography>
                </Container>
            }
            <Container>
                {
                    areas.map(area => (
                        <CardActionArea
                            className="border rounded list-container"
                            key={area._id}
                            onClick={() => fetchDatabase(area._id)}
                        >
                            <Box className="avatar-box">
                                <Avatar variant="square" className="blue-color margin-area-1rem avatar">{area.name.substring(0, 1)}</Avatar>
                                <Typography>
                                    {area.name}
                                </Typography>
                            </Box>
                            <Box>
                                <ArrowForwardIcon />
                            </Box>
                        </CardActionArea>
                    ))
                }
            </Container>
        </>
    );
};

export async function getServerSideProps({ req }) {
    const session = await getSession({ req });

    if (!session) {
        return {
            redirect: {
                destination: "/",
                permanent: false,
            },
            props: {},
        };
    };

    return {
        props: {
            session,
        },
    };
};

export default Area;