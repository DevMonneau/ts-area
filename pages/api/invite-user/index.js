import dbConnect from "../../../lib/dbconnect";

import User from "../../../models/User";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "POST": {
                    return postInviteUser(req, res, session);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const postInviteUser = async (req, res, session) => {
    const { email } = req.body;

    try {
        const findUser = await User.findOne({ email });

        if (!findUser) {
            throw "User not found";
        };

        const getAreaId = await User.findById({ _id: session.userId });
        
        await User.findOneAndUpdate(
            { email },
            {
                "$push":
                    { area: getAreaId.areaSelected }
            });

        return res.status(200).json({
            status: "Success",
        });
    } catch (err) {
        if (err === "User not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
                error: "User not found",
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
