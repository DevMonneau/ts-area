import VideoGame from '../../../models/VideoGame';

import dbConnect from '../../../lib/dbconnect';

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case 'GET': {
                    return getVideoGame(req, res);
                }
                case 'PUT': {
                    return putVideoGame(req, res);
                }
                case 'DELETE': {
                    return deleteVideoGame(req, res);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getVideoGame = async (req, res) => {
    const { id } = req.query;

    try {
        const videoGame = await VideoGame.findById(id[0]);

        if (!videoGame) {
            throw "Video Game not found";
        };

        return res.status(200).json({
            status: "Success",
            videoGame,
        })
    } catch (err) {
        if (err === "Video Game not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(400).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
const putVideoGame = async (req, res) => {
    const { id } = req.query;
    const { game } = await req.body;

    try {
        const videoGame = await VideoGame.findById(id[0]);

        if (!videoGame) {
            throw "Video Game not found";
        };

        const upDatedVideoGame = await VideoGame.findByIdAndUpdate({ _id: id }, { game: game.name });

        return res.status(200).json({
            status: "Success",
            videoGame,
        });
    } catch (err) {
        if (err === "Video Game not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(400).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteVideoGame = async (req, res) => {
    const { id } = req.query;
    try {
        const videoGame = await VideoGame.findByIdAndDelete(id[0]);

        if (!videoGame) {
            throw "Video Game not found";
        };

        return res.status(200).json({
            status: "Success",
            message: "Video Game Deleted Succefully",
        });
    } catch (err) {
        if (err === "Video Game not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(400).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
