import VideoGame from '../../../models/VideoGame';

import dbConnect from '../../../lib/dbconnect';

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
  try {
    await dbConnect();
    const session = await getSession({ req });

    if (session) {
      switch (req.method) {
        case 'GET': {
          return getVideoGame(req, res);
        }
        case 'POST': {
          return postVideoGame(req, res);
        }
      };
    };

    return res.status(401).json({
      status: "Fail",
      message: "Unauthorized",
    });
  } catch (err) {
    return res.status(503).json({
      status: "Fail",
      message: "Service Unavaible",
    });
  };
};

const getVideoGame = async (_req, res) => {
  try {
    let videoGames = await VideoGame.find({});

    return res.status(200).json({
      status: 'Success',
      videoGames,
    });
  } catch (err) {
    return res.status(500).json({
      status: 'Fail',
      message: "Internal Server Error",
    });
  }
};

const postVideoGame = async (req, res) => {
  const { game } = await req.body;

  try {
    const existGame = await VideoGame.findOne({ name: game.name });

    if (existGame) {
      throw "Game already exist";
    };

    let videogame = await VideoGame.create({ name: game.name });

    return res.status(201).json({
      status: 'Success',
      videogame,
    });
  } catch (err) {
    if (err === "Game already exist") {
      return res.status(404).json({
        status: "Fail",
        message: err,
      })
    }
    return res.status(500).json({
      status: 'Fail',
      message: "Internal Server Error",
    });
  };
};
