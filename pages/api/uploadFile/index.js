import dbConnect from "../../../lib/dbconnect";
import GameSettings from "../../gamesettings";
import formidable from "formidable";
import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getUploadFile(req, res);
                }
                case "POST": {
                    return postUploadFile(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postUploadFile = async (req, res) => {
    console.log("inside post method")
    try {
        console.log("inside try")
        const form = new formidable.IncomingForm();
        const uploadFolder = path.join(__dirname, "../pages/images");
        form.multiples = true;
        form.maxFileSize = 50 * 1024 * 1024;
        form.uploadDir = uploadFolder;
        form.parse(req, async (err, fields, files) => {
            console.log("fields", fields);
            console.log("files", files);
            if (err) {
                "Error parsing files";
                return res.status(400).json({
                    status: "Fail",
                    message: "Bad request",
                });
            };
        });
        console.log("form", form);

        return res.status(201).json({
            status: "Success",
            message: "Image successfully uploaded",
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
