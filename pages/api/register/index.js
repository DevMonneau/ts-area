import bcrypt from "bcrypt";

import User from "../../../models/User";

import dbConnect from "../../../lib/dbconnect";

export default async function handle(req, res) {
    try {
        await dbConnect();

        switch (req.method) {
            case "POST": {
                return postUser(req, res);
            }
        };
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const postUser = async (req, res) => {
    const { name, password, email } = req.body;

    try {
        let existUser = await User.find({ email });

        if (!existUser) {
            throw "Email already used";
        };

        const hashedPassword = await bcrypt.hash(password, 12);
        const newUser = await User.create({ name, password: hashedPassword, email, createdAt: new Date() });

        return res.status(201).json({
            status: "Success",
            message: "User created",
            newUser,
        });
    } catch (err) {
        if (err === "Email already used") {
            return res.status(403).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
