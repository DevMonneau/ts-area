import User from "../../../models/User";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getUser(req, res);
                }
                case "PUT": {
                    return putUser(req, res);
                }
                case "DELETE": {
                    return deleteUser(req, res);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getUser = async (req, res) => {
    const { id } = req.query;

    try {
        const user = await User.findById(id[0]);
        console.log(user);

        if (!user) {
            throw "User not found";
        };

        return res.json({
            status: "Success",
            user,
        });
    } catch (err) {
        if (err === "User not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putUser = async (req, res) => {
    const { id } = req.query;
    const { name, email } = await req.body;

    try {
        const findUser = await User.findById(id[0]);

        if (!findUser) {
            throw "User not found";
        };

        const upDateUser = await User.findByIdAndUpdate({ _id: id[0] }, { name, email });

        return res.status(200).json({
            status: "Success",
            before: findUser,
            after: upDateUser,
        });
    } catch (err) {
        if (err === "User not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteUser = async (req, res) => {
    const { id } = req.query;

    try {
        const user = await User.findByIdAndDelete(id[0]);

        if (!user) {
            throw "User not found";
        };

        return res.status(200).json({
            status: "Success",
            message: `${user.name} deleted succefully`,
        });
    } catch (err) {
        if (err === "User not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
