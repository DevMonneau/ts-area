// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import dbconnect from "../../lib/dbconnect";
import { getSession } from "next-auth/react";

export default async function handler(req, res) {
  await dbconnect();
  const session = await getSession({ req });

  res.status(200).json({
    session,
  });
};
