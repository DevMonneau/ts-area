import News from "../../../models/News";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getNews(req, res);
                }
                case "PUT": {
                    return putNews(req, res);
                }
                case "DELETE": {
                    return deleteNews(req, res);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getNews = async (req, res) => {
    const { id } = req.query;

    try {
        const news = await News.findById(id[0]);

        if (!news) {
            throw "News not found";
        };

        return res.status(200).json({
            status: "Success",
            news,
        });
    } catch (err) {
        if (err === "News not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putNews = async (req, res) => {
    const { id } = req.query;
    const { title, body } = await req.body;

    try {
        const existNews = await News.findById(id[0]);

        if (!existNews) {
            throw "News not found";
        };

        const updatedNews = await News.findByIdAndUpdate({ _id: id }, { title, body });

        return res.status(200).json({
            status: "Success",
            before: existNews,
            after: updatedNews,
        });
    } catch (err) {
        if (err === "News not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteNews = async (req, res) => {
    const { id } = req.query;

    try {
        const existNews = await News.findById(id[0]);

        if (!existNews) {
            throw "News not found";
        };

        const deletedNew = await News.findByIdAndDelete(id[0]);

        return res.status(200).json({
            status: "Success",
            deletedNew,
        });
    } catch (err) {
        if (err === "News not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

