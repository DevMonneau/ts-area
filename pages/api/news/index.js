import News from "../../../models/News";
import User from "../../../models/User";

import dbconnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

import moment from "moment";

export default async function handle(req, res) {
    try {
        await dbconnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getNews(req, res, session);
                }
                case "POST": {
                    return postNews(req, res, session);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getNews = async (req, res, session) => {
    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const news = await News.find({ areaId: getAreaId.areaSelected })
            .populate({ path: "userId", model: User });

        const newsByUserId = await News.find({ userId: session.userId })
            .populate({ path: "userId", model: User });

        if (!news) {
            return res.status(204).json({
                status: "Success",
                news: [],
            });
        };

        let newFormat = [];

        for (let i = 0; i < news.length; ++i) {
            let data = {
                _id: news[i]._id,
                body: news[i].body,
                userId: news[i].userId.name,
                postedAt: news[i].postedAt,
                likes: news[i].likes,
                isLiked: news[i].isLiked,
            };
            newFormat.push(data);
        };

        return res.status(200).json({
            status: "Success",
            favoritesNews: getAreaId.favoriteNews,
            news: newFormat,
            newsByUserId,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postNews = async (req, res, session) => {
    const { body } = req.body;

    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const newPost = await News.create({
            body,
            postedAt: new Date(),
            areaId: getAreaId.areaSelected,
            userId: session.userId,
            isLiked: false,
        });

        return res.status(201).json({
            status: "Success",
            newPost,
        });
    } catch (err) {
        console.log("inside catch")
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};
