import NextAuth from 'next-auth';
import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
/* import EmailProvider from 'next-auth/providers/email';
 */import { MongoDBAdapter } from "@next-auth/mongodb-adapter";
import mongoClient from '../../../lib/mongoDb';
import dbConnect from '../../../lib/dbconnect';
import User from "../../../models/User";

export default NextAuth({
  // Next V4 adapter required !!! 
  adapter: MongoDBAdapter(mongoClient),
  // For persisted data, set strategy connection in db (adapter required).
  session: {
    strategy: "database",
    maxAge: 15 * 24 * 60 * 60,
    updateAge: 12 * 60 * 60,
  },
  // Require for production. If not defined the user will be redirect to an error page.
  secret: process.env.SECRET_AUTH,
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SERCRET,
    }),
    // EMAIL PROVIDER TO CONTINUE WITH PIERRE ALEXANDRE
    /*     EmailProvider({
          server: process.env.EMAIL_SERVER,
          clientSecret: process.env.EMAIL_FROM,
        }), */
  ],
  pages: {
    signIn: "/",
    // signOut: "/auth/logout",
    // error: "/auth/error",
    // newUser: "/auth/new-user",
  },
  debug: true,
  callbacks: {
    async session({ session, user }) {
      session.userId = user.id;
      session.favoriteNews = user.favoriteNews;
      session.areaSelected = user.areaSelected;
      return session;
    },
    async signIn({ user }) {
      try {
        await dbConnect();

        if (!user.area) {
          await User.findOneAndUpdate({ email: user.email }, { array: [] }, { new: true });
        };

        return true;
      } catch (err) {
        return false;
      };
    },
  },
});