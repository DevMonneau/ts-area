import Contact from "../../../models/Contact";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getContact(req, res);
                }
                case "PUT": {
                    return putContact(req, res);
                }
                case "DELETE": {
                    return deleteContact(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getContact = async (req, res) => {
    const { id } = await req.query;

    try {
        const contact = await Contact.findById(id[0]);

        if (!contact) {
            throw "Contact not found";
        };

        return res.status(200).json({
            status: "Success",
            contact,
        });
    } catch (err) {
        if (err === "Contact not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putContact = async (req, res) => {
    const { id } = req.query;
    const { twitter, instagram, phoneNumber } = req.body;

    try {
        const existContact = await Contact.findById(id[0]);

        if (!existContact) {
            throw "Contact not found";
        };

        const newContact = await Contact.findByIdAndUpdate({ _id: id }, { twitter, instagram, phoneNumber })

        return res.status(200).json({
            status: "Success",
            befor: existContact,
            after: newContact,
        });
    } catch (err) {
        if (err === "Contact not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            })
        }
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteContact = async (req, res) => {
    const { id } = req.query;

    try {
        const existContact = await Contact.findByIdAndDelete(id[0]);

        if (!existContact) {
            throw "Contact not found";
        };

        return res.status(200).json({
            status: "Success",
            deleted: existContact,
        });
    } catch (err) {
        if (err === "Contact not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
