import Contact from "../../../models/Contact";
import User from "../../../models/User";
import Area from "../../../models/Area";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getContact(req, res, session);
                }
                case "POST": {
                    return postContact(req, res, session);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getContact = async (req, res, session) => {
    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const contacts = await Contact.find({ areaId: getAreaId.areaSelected })
            .populate({ path: "areaId", model: Area })
            .populate({ path: "userId", model: User });

        if (!contacts) {
            return res.status(204).json({
                contacts: [],
            });
        };

        let newFormat = [];

        for (let i = 0; i < contacts.length; ++i) {
            let data = {
                twitter: contacts[i].twitter,
                instagram: contacts[i].instagram,
                phoneNumber: contacts[i].phoneNumber,
                userId: contacts[i].userId.name,
                _id: contacts[i]._id,
            };
            newFormat.push(data);
        };

        return res.status(200).json({
            status: "Success",
            contacts: newFormat,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};

const postContact = async (req, res, session) => {
    const { twitter, instagram, phoneNumber } = req.body;

    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const existContact = await Contact.findOne({ twitter } || { instagram } || { phoneNumber });

        if (existContact) {
            throw "Contact already used"
        };

        const newContact = await Contact.create({
            twitter,
            instagram,
            phoneNumber,
            userId: session.userId,
            areaId: getAreaId.areaSelected,
        });

        return res.status(200).json({
            status: "Success",
            newContact,
        });
    } catch (err) {
        if (err === "Contact already used") {
            return res.status(403).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};
