import Category from "../../../models/Category";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getCategory(req, res);
                }
                case "POST": {
                    return postCategory(req, res);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getCategory = async (_req, res) => {
    try {
        const categories = await Category.find({});

        if (!categories) {
            return res.status(204).json({
                status: "Success",
                data: [],
            });
        };

        return res.status(200).json({
            status: "Success",
            categories,
        });
    } catch (err) {
        return res.json(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postCategory = async (req, res) => {
    const { category } = req.body;

    try {
        const existCategory = await Category.findOne({ name: category.name });

        if (existCategory) {
            throw "Category already exist";
        };

        let newCategory = await Category.create({ name: category.name });

        return res.status(201).json({
            status: "Success",
            newCategory,
        });
    } catch (err) {
        if (err === "Category already exist") {
            return res.status(403).json({
                status: "Fail",
                message: err,
            })
        }
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
