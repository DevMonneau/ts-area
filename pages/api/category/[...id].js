import Category from "../../../models/Category";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getCategory(req, res);
                }
                case "PUT": {
                    return putCategory(req, res);
                }
                case "DELETE": {
                    return deleteCategory(req, res);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getCategory = async (req, res) => {
    const { id } = req.query;

    try {
        const category = await Category.findById(id[0]);

        if (!category) {
            throw "Category not found"
        };

        return res.status(200).json({
            status: "Success",
            category,
        });
    } catch (err) {
        if (err === "Category not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putCategory = async (req, res) => {
    const { id } = req.query;
    const { category } = await req.body;

    try {
        const findCategory = await Category.findById(id[0]);

        if (!category) {
            throw "Category not found";
        };

        const updatedCategory = await Category.findByIdAndUpdate({ _id: id }, { name: category });

        return res.status(200).json({
            status: "Success",
            message: "Category updated",
            before: findCategory,
            after: updatedCategory,
        });
    } catch (err) {
        if (err === "Category not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };
    };

    return res.status(500).json({
        status: "Fail",
        message: "Internal Server Error",
    });
};

const deleteCategory = async (req, res) => {
    const { id } = req.query;

    try {
        const category = await Category.findByIdAndDelete(id[0]);

        if (!category) {
            throw "Category not found";
        };

        return res.status(200).json({
            status: "Success",
            message: "Category deleted succefully",
            category,
        });
    } catch (err) {
        if (err === "Category not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
