import SharedAccount from '../../../models/SharedAccount';
import VideoGame from "../../../models/VideoGame";
import User from "../../../models/User";

import dbconnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbconnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case 'GET': {
                    return getAccounts(req, res, session);
                }
                case 'POST': {
                    return postAccounts(req, res, session);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getAccounts = async (req, res, session) => {
    try {
        const getAreaId = await User.findById({ _id: session.userId });

        let accounts = await SharedAccount
            .find({ areaId: getAreaId.areaSelected })
            .populate({ path: "accountGame", model: VideoGame, select: "name -_id" })
            .populate({ path: "userId", model: User });

        if (!accounts) {
            return res.status(204).json({
                status: "Success",
                data: [],
            });
        };

        let newFormat = [];

        for (let i = 0; i < accounts.length; i++) {
            let data = {
                accountName: accounts[i].accountName,
                accountPwd: accounts[i].accountPwd,
                accountId: accounts[i].accountId,
                accountRank: accounts[i].accountRank,
                accountGame: accounts[i].accountGame.name,
                _id: accounts[i]._id,
                userId: accounts[i].userId.name,
            };
            newFormat.push(data);
        };

        return res.status(200).json({
            status: 'Success',
            newFormat,
        });
    } catch (err) {
        return res.status(500).json({
            status: 'Fail',
            message: 'Internal Server',
        });
    };
};

const postAccounts = async (req, res, session) => {
    const { accountName, accountPwd, accountId, accountRank, accountGame } = req.body;

    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const existAccount = await SharedAccount.findOne({ accountName });

        if (existAccount) {
            throw "This account already exist";
        };

        const newAccount = await SharedAccount.create({
            accountName,
            accountPwd,
            createdAt: new Date(),
            accountId,
            accountRank,
            accountGame,
            userId: session.userId,
            areaId: getAreaId.areaSelected
        });

        return res.status(201).json({
            status: "Success",
            newAccount,
        });
    } catch (err) {
        if (err === "This account already exist") {
            return res.status(403).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};
