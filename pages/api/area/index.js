import Area from "../../../models/Area";
import User from "../../../models/User";

import dbconnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbconnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return await getArea(req, res, session);
                }
                case "POST": {
                    return await postArea(req, res, session);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: 'Unauthorized',
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getArea = async (_req, res, session) => {
    try {
        const user = await User
            .findById({ _id: session.userId })
            .populate({ path: "area", model: Area });

        return res.status(200).json({
            status: "Success",
            areas: user.area,
            areaSelected: user.areaSelected,
        });
    } catch (err) {
        if (err === "Areas not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postArea = async (req, res, session) => {
    const { areaName } = req.body;
    const id = session.userId;

    try {
        const existArea = await Area.findOne({ name: areaName.name });

        if (existArea) {
            throw "Name already exist";
        };

        const newArea = await Area.create({ name: areaName, createdAt: new Date(), createdBy: id, userId: id });
        const idToPush = newArea._id;

        const updatedUser = await User.findByIdAndUpdate(
            { _id: id },
            {
                "$push":
                    { area: idToPush },
            },
            { new: true });

        return res.status(201).json({
            status: "Success",
            updatedUser,
        });
    } catch (err) {
        if (err === "Name already exist") {
            return res.status(403).json({
                status: "Fail",
                message: "This area name is already used",
            });
        }
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};
