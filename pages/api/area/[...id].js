import Area from "../../../models/Area";
import User from "../../../models/User";
import SharedAccount from "../../../models/SharedAccount";
import Link from "../../../models/Links";
import Tuto from "../../../models/Tuto";
import Contact from "../../../models/Contact";
import News from "../../../models/News";

import dbconnect from "../../../lib/dbconnect";
import mongoose from "mongoose";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbconnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getArea(req, res, session);
                }
                case "PUT": {
                    return putArea(req, res, session);
                }
                case "PATCH": {
                    return patchArea(req, res, session);
                }
                case "DELETE": {
                    return deleteArea(req, res, session);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getArea = async (req, res, session) => {
    const { id } = req.query;

    try {
        const area = await Area.findById(id[0]);

        await User.findByIdAndUpdate({ _id: session.userId }, { areaSelected: req.headers.areaid });

        if (!area) {
            throw "Area not found";
        };

        return res.status(200).json({
            status: "Success",
            area,
        });
    } catch (err) {
        if (err === "Area not found") {
            res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};

const putArea = async (req, res) => {
    const { id } = req.query;
    const { name } = await req.body;

    try {
        const existArea = await Area.findById(id[0]);

        if (!existArea) {
            throw "Area not found";
        };

        const modifyArea = await Area.findByIdAndUpdate({ _id: id }, name);

        return res.status(200).json({
            status: "Success",
            message: "Name updated",
            before: existArea,
            after: modifyArea,
        });
    } catch (err) {
        if (err === "Area not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const patchArea = async (req, res, session) => {
    const { id } = req.query;

    try {
        const user = await User.findOne({ _id: session.userId })

        if (!user) {
            throw "User not found"
        };

        for (let i = 0; i < user.area.length; ++i) {
            if (user.area[i].toString() === id[0]) {
                user.area.splice(i, 1);
                user.save();
            };
        };

        return res.status(200).json({
            status: "Success",
        });
    } catch (err) {
        if (res === "User not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteArea = async (req, res, session) => {
    const { id } = req.query;

    try {
        const user = await User.findOne({ _id: session.userId })

        if (!user) {
            throw "User not found"
        };

        await SharedAccount.deleteMany({
            "areaId": {
                $in: mongoose.Types.ObjectId(id[0]),
            },
        });

        await Link.deleteMany({
            "areaId": {
                $in: mongoose.Types.ObjectId(id[0]),
            },
        });

        await Tuto.deleteMany({
            "areaId": {
                $in: mongoose.Types.ObjectId(id[0]),
            },
        });

        await Contact.deleteMany({
            "areaId": {
                $in: mongoose.Types.ObjectId(id[0]),
            },
        });

        await News.deleteMany({
            "areaId": {
                $in: mongoose.Types.ObjectId(id[0]),
            },
        });

        for (let i = 0; i < user.area.length; ++i) {
            if (user.area[i].toString() === id[0]) {
                user.area.splice(i, 1);
                user.save();
            };
        };

        const area = await Area.findByIdAndDelete(id[0]);

        return res.status(200).json({
            status: "Success",
            message: `${area.name} was deleted`,
        });
    } catch (err) {
        if (err === "User not found") {
            console.log("area not found")
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
