import dbConnect from "../../../lib/dbconnect";
import GameSettings from "../../gamesettings";
import { getSession } from "next-auth/react";
import formidable from "formidable";
import fs from "fs";
import path from "path";

export const config = {
    api: {
        bodyParser: false
    }
};

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getGameSettings(req, res);
                }
                case "PUT": {
                    return putGameSettings(req, res);
                }
                case "PATCH": {
                    return patchGameSettings(req, res);
                }
                case "DELETE": {
                    return deleteGameSettings(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service unavaible",
        });
    };
};

const getGameSettings = async (req, res) => {
    try {

    } catch (err) {

    };
};

const putGameSettings = async (req, res) => {
    try {

    } catch (err) {

    };
};

const patchGameSettings = async (req, res) => {
    // faire un dossier externe dans l'api avec un lien (pages/img/userId/image)
    console.log(req)
    try {
        let imgname = req.query.id[0];
        console.log("imgname", imgname);
        let form = new formidable.IncomingForm();
        // ajouter l'extension dans uploadDir
        form.uploadDir = `/public/images/${imgname}`;
        console.log("new formidable IncomingForm", form)
        form.parse(req, (err, fields, files) => {
            console.log("form parse")
            return res.status(204).json({
                status: "Success",
                message: "Resource updated successfully",
                form,
            });
        })
        form.on('fileBegin', (name, file) => {
            console.log("name", name);
            console.log("file", file);
            console.log("form on")
            file.path = path.join(__dirname, `/public/images/${imgname}`);
            imgname = file.name;
        })
        return res.status(204).json({
            status: "Success",
            message: "Resource updated successfully",
            form,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

