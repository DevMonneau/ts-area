import dbConnect from "../../../lib/dbconnect";
import GameSettings from "../../../models/GameSettings";
import Area from "../../../models/Area";
import User from "../../../models/User";
import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getGameSettings(req, res);
                }
                case "POST": {
                    return postGameSettings(req, res, session);
                };
                case "PATCH": {
                    return patchGameSettings(req, res, session);
                }
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Server error",
        });
    };
};

const getGameSettings = async (req, res) => {
    try {
        const gameSettings = await GameSettings
            .find({})
            .populate({ path: "areaId", model: Area })
            .populate({ path: "userId", model: User });

        if (!gameSettings) {
            return res.status(204).json({
                status: "Success",
                gameSettings: [],
            });
        };

        return res.status(200).json({
            status: "Success",
            gameSettings,
        });
    } catch (err) {
        return res.status(500).json({
            status: 'Fail',
            message: 'Internal Server Error',
        });
    };
};

const postGameSettings = async (req, res, session) => {
    const { image } = req.body;
    
    try {
        const gameSettings = await GameSettings.create({
            image,
            areaId: session.areaSelected,
            userId: session.userId,
        });

        return res.status(201).json({
            status: "Success",
            message: "Game settings created",
            gameSettings,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
