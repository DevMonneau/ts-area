
import Tuto from "../../../models/Tuto";
import Category from "../../../models/Category";
import User from "../../../models/User";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getTuto(req, res, session);
                };
                case "POST": {
                    return postTuto(req, res, session);
                };
            };
        };

        return res.status(204).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getTuto = async (req, res, session) => {
    try {
        const getAreaId = await User.findById({ _id: session.userId });
        const tutos = await Tuto
            .find({ areaId: getAreaId.areaSelected })
            .populate({ path: "category", model: Category, select: "name -_id" })
            .populate({ path: "userId", model: User });

        if (!tutos) {
            return res.status(204).json({
                status: "Success",
                newFormat: [],
            });
        };

        let newFormat = [];

        for (let i = 0; i < tutos.length; i++) {
            let data = {
                link: tutos[i].link,
                category: tutos[i].category.name,
                _id: tutos[i]._id,
                userId: tutos[i].userId.name,
            };
            newFormat.push(data);
        };

        return res.status(200).json({
            status: "Success",
            newFormat,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postTuto = async (req, res, session) => {
    const { link, category } = req.body;

    try {
        const getAreaId = await User.findById({ _id: session.userId });

        const newTuto = await Tuto.create({
            link,
            category,
            userId: session.userId,
            areaId: getAreaId.areaSelected,
        });

        return res.status(201).json({
            status: "Success",
            message: "Tuto created",
            newTuto,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
