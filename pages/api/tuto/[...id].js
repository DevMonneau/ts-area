import Tuto from "../../../models/Tuto";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getTuto(req, res);
                };
                case "PUT": {
                    return putTuto(req, res);
                };
                case "DELETE": {
                    return deleteTuto(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getTuto = async (req, res) => {
    const { id } = await req.query;

    try {
        const existTuto = await Tuto.findById(id[0]);

        if (!existTuto) {
            throw "Tuto not found";
        };

        return res.status(200).json({
            status: "Success",
            data: existTuto,
        });
    } catch (err) {
        if (err === "Tuto not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putTuto = async (req, res) => {
    const { id } = await req.query;
    const { link } = await req.body;

    try {
        const existTuto = await Tuto.findById(id[0]);

        if (!existTuto) {
            throw "Tuto not found";
        };

        const upDatedTuto = await Tuto.findByIdAndUpdate({ _id: id }, { link });

        return res.status(200).json({
            status: "Success",
            message: "Tuto updated",
            before: existTuto,
            after: upDatedTuto,
        });
    } catch (err) {
        if (err === "Tuto not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteTuto = async (req, res) => {
    const { id } = req.query;

    try {
        const existTuto = await Tuto.findByIdAndDelete({ _id: id });

        if (!existTuto) {
            throw "Tuto not found";
        };

        return res.status(200).json({
            status: "Success",
            deleted: existTuto,
        });
    } catch (err) {
        if (err === "tuto not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
