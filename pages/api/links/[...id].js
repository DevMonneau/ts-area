import Link from "../../../models/Links";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/client";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getLink(req, res);
                }
                case "PUT": {
                    return putLink(req, res);
                }
                case "DELETE": {
                    return deleteLink(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getLink = async (req, res) => {
    const { id } = req.query;

    try {
        const links = await Link.findById(id[0]);

        if (!links) {
            throw "Link not found";
        };

        return res.status(200).json({
            status: "Success",
            links,
        });
    } catch (err) {
        if (err === "Link not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};

const putLink = async (req, res) => {
    const { id } = req.query;
    const { link } = await req.body;

    try {
        const existLink = await Link.findById(id[0])

        if (!existLink) {
            throw "Link not found";
        };

        const newLink = await Link.findByIdAndUpdate({ _id: id }, link);

        return res.status(200).json({
            status: "Success",
            message: "Link updated",
            before: existLink,
            after: newLink,
        });
    } catch (err) {
        if (err === "Link not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteLink = async (req, res) => {
    const { id } = req.query;

    try {
        const link = await Link.findByIdAndDelete(id[0]);

        if (!link) {
            throw "Link not found"
        }

        return res.status(200).json({
            status: "Success",
            message: "Link Deleted Succefully",
            deleted: link,
        });
    } catch (err) {
        if (err === "Link not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
