import Link from "../../../models/Links";
import Category from "../../../models/Category";
import User from "../../../models/User";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getLink(req, res, session);
                }
                case "POST": {
                    return postLink(req, res, session);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getLink = async (req, res, session) => {
    try {
        const getAreaId = await User.findById({ _id: session.userId });

        const links = await Link
            .find({ areaId: getAreaId.areaSelected })
            .populate({ path: "category", model: Category, select: "name -_id" })
            .populate({ path: "userId", model: User });

        if (!links) {
            return res.status(204).json({
                status: "Success",
                newFormat: [],
            });
        };

        let newFormat = [];

        for (let i = 0; i < links.length; ++i) {
            let data = {
                _id: links[i]._id,
                link: links[i].link,
                category: links[i].category.name,
                userId: links[i].userId.name,
            };
            newFormat.push(data);
        };

        return res.status(200).json({
            status: "Success",
            newFormat,
        });
    } catch (err) {

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};

const postLink = async (req, res, session) => {
    const { link, category } = req.body;

    try {
        const getAreaId = await User.findById({ _id: session.userId });

        const newLink = await Link.create({
            link,
            category,
            userId: session.userId,
            areaId: getAreaId.areaSelected,
        });

        return res.status(201).json({
            status: "Success",
            newLink,
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
            err,
        });
    };
};
