import dbConnect from "../../../lib/dbconnect";
import { getSession } from "next-auth/react";
import News from "../../../models/News";
import User from "../../../models/User";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "POST": {
                    return postCount(req, res, session.userId);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Server error",
        });
    };
};

const postCount = async (req, res, userId) => {
    const { id } = req.query;
    try {
        const findNews = await News.findById({ _id: id });
        let previousLikes = findNews.likes;
        await News.findByIdAndUpdate({ _id: id }, { likes: previousLikes + 1, isLiked: true });
        await User.findByIdAndUpdate(
            { _id: userId },
            {
                "$push":
                    { favoriteNews: id },
            },
            { new: true },
        );

        return res.status(200).json({
            status: "Success",
        });
    } catch (err) {
        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
