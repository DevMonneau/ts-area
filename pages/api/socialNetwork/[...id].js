import SocialNetwork from "../../../models/SocialNetwork";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getSocialNetwork(req, res);
                }
                case "PUT": {
                    return putSocialNetwork(req, res);
                }
                case "DELETE": {
                    return deleteSocialNetwork(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getSocialNetwork = async (req, res) => {
    const { id } = req.query;

    try {
        const socialNetwork = await SocialNetwork.findById(id[0]);

        if (!socialNetwork) {
            throw "Social Network not found";
        };

        return res.status(200).json({
            status: "Success",
            socialNetwork,
        });
    } catch (err) {
        if (err === "Social Network not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const putSocialNetwork = async (req, res) => {
    const { id } = req.query;
    const { socialNetwork } = await req.body;

    try {
        const findSocialNetwork = await SocialNetwork.findById(id[0]);

        if (!findSocialNetwork) {
            throw "Social Network not found";
        };

        const upDatedDocument = await SocialNetwork.findByIdAndUpdate({ _id: id[0] }, { name: socialNetwork.name });

        return res.status(200).json({
            status: "Success",
            before: findSocialNetwork,
            after: upDatedDocument,
        })
    } catch (err) {
        if (err === "Social Network not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const deleteSocialNetwork = async (req, res) => {
    const { id } = req.query;

    try {
        const socialNetwork = await SocialNetwork.findByIdAndDelete(id[0]);

        if (!socialNetwork) {
            throw "Social Network not found";
        };

        return res.status(200).json({
            status: "Success",
            message: `${socialNetwork.name} deleted succefully`,
        });
    } catch (err) {
        if (err === "Social Network not found") {
            return res.status(404).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};
