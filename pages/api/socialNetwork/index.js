import SocialNetwork from "../../../models/SocialNetwork";

import dbConnect from "../../../lib/dbconnect";

import { getSession } from "next-auth/react";

export default async function handle(req, res) {
    try {
        await dbConnect();
        const session = await getSession({ req });

        if (session) {
            switch (req.method) {
                case "GET": {
                    return getSocialNetwork(req, res);
                }
                case "POST": {
                    return postSocialNetwork(req, res);
                };
            };
        };

        return res.status(401).json({
            status: "Fail",
            message: "Unauthorized",
        });
    } catch (err) {
        return res.status(503).json({
            status: "Fail",
            message: "Service Unavaible",
        });
    };
};

const getSocialNetwork = async (_req, res) => {
    try {
        const socialNetwork = await SocialNetwork.find({});

        if (!socialNetwork) {
            return res.status(204).json({
                status: "Success",
                data: [],
            });
        };

        res.status(200).json({
            status: "Success",
            socialNetwork,
        });
    } catch (err) {
        return res.json(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};

const postSocialNetwork = async (req, res) => {
    const { socialNetwork } = req.body;

    try {
        const existSocialNetwork = await SocialNetwork.findOne({ name: socialNetwork.name });

        if (existSocialNetwork) {
            throw "Social Network already exist";
        };

        let newSocialNetwork = await SocialNetwork.create({ name: socialNetwork.name });

        return res.status(201).json({
            status: "Success",
            newSocialNetwork,
        });
    } catch (err) {
        if (err === "Social Network already exist") {
            return res.status(403).json({
                status: "Fail",
                message: err,
            });
        };

        return res.status(500).json({
            status: "Fail",
            message: "Internal Server Error",
        });
    };
};