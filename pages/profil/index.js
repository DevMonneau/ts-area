import AppBar from "../../components/layout/appbar";
import Head from "next/head";
import {
    Container,
    Typography,
    Avatar,
    Card,
    CardContent,
    Grid,
    Box,
} from "@mui/material";
import { useSession } from "next-auth/react";

const Profil = () => {
    const { data: session, status } = useSession();

    if (!session) {
        return (
            <div>Loading...</div>
        );
    };

    return (
        <div>
            <Head>
                <title>TS AREA - PROFIL</title>
            </Head>

            <AppBar />
            <Container coponent="main" sx={{ display: "flex", flexDirection: "column", width: 400, alignItems: "center" }}>
                <Grid>
                    <Card elevation={5}>
                        <CardContent
                            sx={{ backgroundColor: "#424242", height: 50, color: "white", textAlign: "center" }}
                        >
                            <Typography>Personnal Informations</Typography>
                        </CardContent>
                        <CardContent sx={{ display: "flex", justifyContent: "center" }}>
                            <Avatar alt={session.user.name} src={session.user.image} sx={{ width: 100, height: 100 }} />
                        </CardContent>
                        <CardContent sx={{ display: "flex" }}>
                            <CardContent>
                                <Typography>Name</Typography>
                                <Typography>Email</Typography>
                            </CardContent>
                            <CardContent>
                                <Typography>{session.user.name}</Typography>
                                <Typography>{session.user.email}</Typography>
                            </CardContent>
                        </CardContent>
                    </Card>
                </Grid>
            </Container>
        </div>
    );
};

export default Profil;