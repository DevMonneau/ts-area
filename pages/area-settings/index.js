import { useState, useEffect } from "react";
import { server } from "../../tools";
import { useRouter } from "next/dist/client/router";
import { useSession } from "next-auth/react";
import axios from "axios";
import Head from "next/head"
import AppBar from "../../components/layout/appbar";
import {
    Container,
    Box,
    Typography,
    TextField,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
} from "@mui/material";
import ErrorAlert from "../../components/alert/error";

const AreaSettings = () => {
    const [email, setEmail] = useState();
    const [idToDelete, setIdToDelete] = useState();
    const [areaCreatedBy, setArea] = useState();
    const [error, setError] = useState();
    const [open, setOpen] = useState(false);
    const router = useRouter();
    const { data: session, status } = useSession();

    useEffect(() => {
        axios.get(`${server}/api/area`, { withCredentials: true })
            .then(res => {
                setIdToDelete(res.data.areaSelected);
            });
    }, []);

    /* PROBLEME ERREUR 500 AVEC CETTE REQUÊTE !!!!! */
    useEffect(() => {
        axios.get(`${server}/api/area/${idToDelete}`, { withCredentials: true })
            .then(res => {
                console.log("then promise");
                setArea(res.data.area.createdBy);
            });
    }, [idToDelete]);

    const preventDefault = (e) => {
        e.preventDefault();
    };
    
    const handleInvite = async () => {
        await axios.post(`${server}/api/invite-user`, { email }, { withCredentials: true })
            .then(res => {
                if (res.data.status === "Success") {
                    router.push("/dashboard");
                } else {
                    console.log("inside error")
                    setError("User not found");
                };
            });
    };

    const handleClose = () => {
        setOpen(false);
    };
    
    const handleDelete = async () => {
        const res = await axios.delete(`${server}/api/area/${idToDelete}`, { withCredentials: true });
        if (res.data.status === "Success") {
            router.push("/area");
        };
    };

    const handleLeave = async () => {
        await axios.patch(`${server}/api/area/${idToDelete}`, { withCredentials: true })
            .then(res => {
                if (res.data.status === "Success") {
                    router.push("/area");
                };
            });
    };

    return (
        <>
            <Head>
                <title>TS AREA - Area Settings</title>
            </Head>

            <AppBar />
            <Container component="main">
                {
                    error &&
                    <ErrorAlert error={error} />
                }
                <Box>
                    <Box sx={{ display: "flex", flexDirection: "column" }} onSubmit={preventDefault}>
                        <Typography variant="h5" className="margin-1rem">To invite a guest in this area, please enter his email just here :</Typography>
                        <TextField
                            fullWidth
                            className="margin-1rem"
                            id="email"
                            label="Email"
                            variant="outlined"
                            size="small"
                            onChange={e => setEmail(e.target.value)}
                        />
                    </Box>
                    {
                        session.userId === areaCreatedBy
                            ?
                            <Box sx={{ display: "flex", justifyContent: "center", mt: 10 }}>
                                <Button variant="contained" onClick={handleDelete}>Delete this area</Button>
                            </Box>
                            :
                            <Box sx={{ display: "flex", justifyContent: "center", mt: 10 }}>
                                <Button variant="contained" onClick={handleLeave}>Leave this area</Button>
                            </Box>

                    }
                </Box>
            </Container>
        </>
    );
};

AreaSettings.auth = true;

export default AreaSettings;