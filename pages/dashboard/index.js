import { useState, useEffect } from "react";
import Head from "next/head";
import AppBar from "../../components/layout/appbar";
import { useSession } from "next-auth/react";
import axios from "axios";
import { server } from "../../tools";
import {
    Box,
    Avatar,
    FormControl,
    InputLabel,
    Input,
    Button,
} from "@mui/material";
import CardNews from "../../components/layout/card/index";

const Dashboard = () => {
    const [news, setNews] = useState([]);
    const [body, setBody] = useState();
    const [favoritesNews, setFavoritesNews] = useState([]);
    const { data: session, status } = useSession();

    useEffect(() => {
        axios.get(`${server}/api/news`, { withCredentials: true })
            .then(res => {
                console.log(res.data)
                setNews(res.data.news);
                setFavoritesNews(res.data.favoritesNews);
            });
    }, []);

    const handleSubmit = () => {
        axios.post(`${server}/api/news`, { body }, { withCredentials: true })
            .then(res => {
                if (res.data.status === "Success") {
                    axios.get(`${server}/api/news`), { withCredentials: true }
                };
            });
    };

    if (!news) {
        return (
            <div>Loading...</div>
        );
    };

    return (
        <>
            <Head>
                <title>TS AREA - DASHBOARD</title>
            </Head>

            <AppBar>
                <Box component="main" className="dashboard-container" >
                    <Box>
                        <Box sx={{ my: 3, mx: 2 }}>
                            <Avatar alt={session.user.name} src={session.user.image} />
                            <FormControl variant="standard">
                                <InputLabel htmlFor="standard-adornment-amount">What s new</InputLabel>
                                <Input
                                    sx={{ width: "30rem" }}
                                    id="standard-adornment-amount"
                                    onChange={e => setBody(e.target.value)}
                                />
                            </FormControl>
                        </Box>
                        <Box sx={{ mx: 2 }}>
                            <Button variant="contained" size="small" onClick={handleSubmit}>Post</Button>
                        </Box>
                    </Box>
                    <Box className="card-container">
                        {
                            news.map(item => (
                                <CardNews key={item._id} news={item} session={session} favoritesNews={favoritesNews} />
                            ))
                        }
                    </Box>
                </Box>
            </AppBar>
        </>
    );
};

Dashboard.auth = true;

export default Dashboard;