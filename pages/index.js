import { useState } from 'react';
import Loading from '../components/layout/loading';
// Next
import Image from 'next/image';
import Head from 'next/head';
import { getProviders, signIn, useSession, getSession } from 'next-auth/react';
// Import images
import homebg from "../public/homebg.jpg";
// Import icons
import {
  faFacebook,
  faGoogle,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// Import material mui composants
import { Button, Grid, Stack, CssBaseline, TextField, FormControlLabel, Checkbox, Link, Box, Typography, ThemeProvider, createTheme } from '@mui/material';

const iconBrands = {
  Google: faGoogle,
  Facebook: faFacebook,
  Twitter: faTwitter,
};

const theme = createTheme({
  palette: {
    primary: {
      main: "#505050",
    },
  },
});

const Home = ({ providers }) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const { data: session, status } = useSession();

  const preventDefault = (e) => {
    e.preventDefault();
  };

  const handleSubmit = () => {

  };

  if (status === "loading") {
    return <Loading />;
  };

  return (
    <>
      <Head>
        <title>TS AREA</title>
      </Head>

      <Stack>
        <ThemeProvider theme={theme}>
          <Grid container component="main" alignItems="center">
            <CssBaseline />
            <Grid
              item
              xs={false}
              sm={4}
              md={7}
              sx={{
                backgroundImage: 'homebg.jpg',
                backgroundRepeat: 'no-repeat',
                backgroundColor: (t) =>
                  t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                backgroundSize: 'cover',
                backgroundPosition: 'center',
              }}>
              <Image src={homebg} alt="background-image" />
            </Grid>
            <Grid item xs={12} sm={8} md={5}>
              <Box
                sx={{
                  my: 8,
                  mx: 4,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                }}
              >
                <Typography component="h1" variant="h5">
                  Sign in
                </Typography>
                <Box component="form" noValidate onSubmit={preventDefault} sx={{ mt: 1 }}>
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                  />
                  <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label="Remember me"
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                    onClick={handleSubmit}
                  >
                    Sign In
                  </Button>
                  <Grid container justifyContent="space-evenly">
                    {Object.values(providers).map((provider) => (
                      <Grid key={provider.name} sx={{ m: 2 }}>
                        <Button
                          onClick={() => signIn(provider.id)}
                          variant='contained'
                          endIcon={
                            <FontAwesomeIcon icon={iconBrands[provider.name]} />
                          }
                        >
                          {provider.name}
                        </Button>
                      </Grid>
                    ))}
                  </Grid>
                  <Grid container>
                    <Grid item xs>
                      <Link href="#" variant="body2">
                        Forgot password?
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/register" variant="body2">
                        {"Don't have an account? Sign Up"}
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </ThemeProvider>
      </Stack>
    </>
  );
};

export async function getServerSideProps({ req }) {
  const session = await getSession({ req });

  if (session) {
    return {
      redirect: {
        destination: "/area",
        permanent: false,
      },
      props: {},
    };
  };

  const providers = await getProviders();

  return {
    props: {
      providers,
    },
  };
};

export default Home;
