import { useState, useEffect } from "react";
import { server } from "../../tools";
import AppBar from "../../components/layout/appbar";
import TableData from "../../components/layout/table/accountTable";
import {
    Button,
    CssBaseline,
    TextField,
    Grid,
    Container,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Box,
} from "@mui/material";
import {
    GridToolbarDensitySelector,
    GridToolbarFilterButton,
} from '@mui/x-data-grid';
import axios from "axios";
import PropTypes from 'prop-types';

const escapeRegExp = (value) => {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
};

const QuickSearchToolbar = () => {
    return (
        <Box
            sx={{
                p: 0.5,
                pb: 0,
                justifyContent: 'space-between',
                display: 'flex',
                alignItems: 'flex-start',
                flexWrap: 'wrap',
            }}
        >
            <div>
                <GridToolbarFilterButton />
                <GridToolbarDensitySelector />
            </div>
        </Box>
    );
};

QuickSearchToolbar.propTypes = {
    clearSearch: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};
export default function Account() {
    const [accountGame, setAccountGame] = useState();
    const [accountName, setAccountName] = useState();
    const [accountPwd, setAccountPwd] = useState();
    const [accountId, setAccountId] = useState();
    const [accountRank, setAccountRank] = useState();
    const [searchText, setSearchText] = useState();
    const [videogame, setVideoGame] = useState([]);
    const [accounts, setAccounts] = useState([]);

    useEffect(() => {
        axios.get(`${server}/api/videogame`, { withCredentials: true })
            .then(res => {
                setVideoGame(res.data.videoGames);
            });
        axios.get(`${server}/api/account`, { withCredentials: true })
            .then(res => {
                setAccounts(res.data.newFormat);
            });
    }, []);

    const requestSearch = (searchValue) => {
        setSearchText(searchValue);
        const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
        const filteredRows = accounts.filter((row) => {
            return Object.keys(row).some((field) => {
                return searchRegex.test(row[field].toString());
            });
        });
    };

    const preventDefault = (e) => {
        e.preventDefault();
    };

    const handleSubmit = () => {
        const fetchDatabase = async () => {
            const res = await axios.post(
                `${server}/api/account`,
                { accountName, accountPwd, accountId, accountRank, accountGame },
                { withCredentials: true },
            );
            if (res.data.status === "Success") {
                axios.get(`${server}/api/account`, { withCredentials: true })
                    .then(res => {
                        setAccounts(res.data.newFormat);
                    })
            };
        };
        fetchDatabase();
    };

    return (
        <AppBar>
            <Container component="main">
                <CssBaseline />
                <Grid container
                    spacing={0}
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    onSubmit={preventDefault}
                >
                    <Grid sx={{ m: 1 }}>
                        <FormControl sx={{ width: 100 }}>
                            <InputLabel id="videoGame">Video Game</InputLabel>
                            <Select
                                defaultValue
                                labelId="videoGame"
                                id="videoGame"
                                value={accountGame}
                                label="Video Game"
                                onChange={e => setAccountGame(e.target.value)}
                                fullWidth
                            >
                                {
                                    videogame.map(game => (
                                        <MenuItem value={game._id} key={game._id}>{game.name}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid sx={{ m: 1 }}>
                        <TextField id="outlined-basic" label="Account" variant="outlined" onChange={e => setAccountName(e.target.value)} />
                    </Grid>
                    <Grid sx={{ m: 1 }}>
                        <TextField id="outlined-basic" label="Password" variant="outlined" onChange={e => setAccountPwd(e.target.value)} />
                    </Grid>
                    <Grid sx={{ m: 1 }}>
                        <TextField id="outlined-basic" label="Account ID" variant="outlined" onChange={e => setAccountId(e.target.value)} />
                    </Grid>
                    <Grid sx={{ m: 1 }}>
                        <TextField id="outlined-basic" label="Rank" variant="outlined" onChange={e => setAccountRank(e.target.value)} />
                    </Grid>
                    <Grid sx={{ m: 1 }}>
                        <Button variant="contained" size="medium" onClick={handleSubmit}>Send</Button>
                    </Grid>
                </Grid>
                <Grid sx={{ mt: 5 }}>
                    <TableData accounts={accounts} requestSearch={requestSearch} QuickSearchToolbar={QuickSearchToolbar} searchText={searchText} />
                </Grid>
            </Container>
        </AppBar >
    );
};

Account.auth = true;