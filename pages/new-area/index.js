import { useState } from "react";
import Head from "next/head";
import Image from "next/image";
import {
    Container,
    Typography,
    TextField,
    Button,
    Box,
    Tooltip,
} from "@mui/material";
import { useSession } from "next-auth/react";
import axios from "axios";
import { server } from "../../tools";
import { useRouter } from 'next/dist/client/router';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

const NewArea = () => {
    const [areaName, setAreaName] = useState();
    const [userId, setUserId] = useState();
    const [resStatus, setStatus] = useState();
    const { data: session, status } = useSession();
    const router = useRouter();

    const preventDefault = (e) => {
        e.preventDefault();
    };

    const handleSubmit = () => {
        setUserId(session.userId);

        const fetchDatabase = async () => {
            const res = await axios.post(
                `${server}/api/area`,
                { areaName, createdBy: userId },
            )
            console.log(res.data.status)
            if (res.data.status === "Success") {
                setStatus(res.data.status);
                router.push("/area");
            } else {
                setStatus(res.data.status);
            };
        };
        fetchDatabase();
    };

    console.log("status du resultat", resStatus)

    return (
        <>
            <Head>
                <title>TS AREA - New area</title>
            </Head>

            <Box component="main" onSubmit={preventDefault} className="grey-bg newarea-container">
                <Image alt="Mountains" src="/tsarealogo.png" height={200} width={200} />
                <Container className="items-container">
                    <Typography variant="h3" className="margin-1rem">What is the name of your new area ?</Typography>
                    <Typography variant="h5" className="margin-1rem">It will be the name of you space.</Typography>
                    <TextField
                        fullWidth
                        id="name"
                        label="Area name"
                        variant="outlined"
                        size="small"
                        className="margin-1rem"
                        onChange={e => setAreaName(e.target.value)}
                        {
                        ...resStatus === "Fail" && error
                        }
                    />
                    <Button
                        variant="contained"
                        onClick={handleSubmit}
                        className="margin-1rem"
                    >Create an area
                    </Button>
                </Container>
                <Container className="icon-container">
                    <Tooltip title="Go back">
                        <ArrowCircleLeftIcon
                            className="left-icon"
                            onClick={() => { router.push("/area") }}
                        />
                    </Tooltip>
                </Container>
            </Box>
        </>
    );
};

export default NewArea;