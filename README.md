This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Dev dependencies:

- docker
- docker-compose (version 2 only)
- node
- yarn

Install project dependencies:

```bash
yarn
# or
yarn install
```

Do not use npm instead of yarn. Easier to build and deploy app with yarn.

Run the development server:

```bash
yarn dev
```

All Command:

- Docker:

```bash
# start all docker
yarn docker-up

# stop all docker
yarn docker-stop

# delete all container
yarn docker-prune

# connect to db container
yarn docker-shell

# build docker image of the project
yarn docker-build

# deploy docker image built
yarn docker-deploy
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
