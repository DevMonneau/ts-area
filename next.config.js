const config = {
    api: {
        bodyParser: {
            sizeLimit: '1mb',
        },
    },
    images: {
        domains: ["localhost"]
    }
};

module.exports = config;